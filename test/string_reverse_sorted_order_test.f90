! Copyright (c) 2022 Archaeologic, Inc., Brad Richardson
! This software was developed for the U.S. Nuclear Regulatory Commission (US NRC) under contract # 31310020D0006:
! "Technical Assistance in Support of NRC Nuclear Regulatory Research for Materials, Waste, and Reactor Programs"
module string_reverse_sorted_order_test
    use iso_varying_string, only: varying_string, operator(//), var_str
    use sortff, only: reverse_sorted_order
    use veggies, only: &
            result_t, &
            test_item_t, &
            assert_equals, &
            describe, &
            it

    implicit none
    private
    public :: test_reverse_sorted_order
contains
    function test_reverse_sorted_order() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
            "reverse_sorted_order", &
            [ it("returns an empty array given an empty array", check_empty) &
            , it("returns the array [1] given a single element array", check_single) &
            , it( &
                "returns the arrays [1, 2] and [2, 1] for revsere sorted and reverse unsorted 2 element arrays respectively", &
                check_doubles) &
            , it("is stable for 2 element arrays, in that it returns [1, 2] for an array with equal values", &
                check_stable_2_element) &
            , it( &
                "properly sorts the permutations of a 3 element array", &
                check_triples) &
            , it("is stable for longer arrays", check_stable_longer) &
            ])
    end function

    function check_empty() result(result_)
        type(result_t) :: result_

        result_ = assert_equals([integer::], reverse_sorted_order([varying_string::]))
    end function

    function check_single() result(result_)
        type(result_t) :: result_

        result_ = assert_equals([1], reverse_sorted_order([var_str("A")]))
    end function

    function check_doubles() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals([1, 2], reverse_sorted_order([var_str("B"), var_str("A")])) &
                .and.assert_equals([2, 1], reverse_sorted_order([var_str("A"), var_str("B")]))
    end function

    function check_stable_2_element() result(result_)
        type(result_t) :: result_

        result_ = assert_equals([1, 2], reverse_sorted_order([var_str("A"), var_str("A")]))
    end function

    function check_triples() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals([3, 2, 1], reverse_sorted_order([var_str("A"), var_str("B"), var_str("C")])) &
                .and.assert_equals([2, 3, 1], reverse_sorted_order([var_str("A"), var_str("C"), var_str("B")])) &
                .and.assert_equals([3, 1, 2], reverse_sorted_order([var_str("B"), var_str("A"), var_str("C")])) &
                .and.assert_equals([2, 1, 3], reverse_sorted_order([var_str("B"), var_str("C"), var_str("A")])) &
                .and.assert_equals([1, 3, 2], reverse_sorted_order([var_str("C"), var_str("A"), var_str("B")])) &
                .and.assert_equals([1, 2, 3], reverse_sorted_order([var_str("C"), var_str("B"), var_str("A")]))
    end function

    function check_stable_longer() result(result_)
        type(result_t) :: result_

        result_ = &
                assert_equals([4, 3, 1, 2], reverse_sorted_order([var_str("A"), var_str("A"), var_str("B"), var_str("C")])) &
                .and.assert_equals([4, 2, 3, 1], reverse_sorted_order([var_str("A"), var_str("B"), var_str("B"), var_str("C")])) &
                .and.assert_equals([3, 4, 2, 1], reverse_sorted_order([var_str("A"), var_str("B"), var_str("C"), var_str("C")])) &
                .and.assert_equals([3, 4, 1, 2], reverse_sorted_order([var_str("A"), var_str("A"), var_str("C"), var_str("B")])) &
                .and.assert_equals([2, 3, 4, 1], reverse_sorted_order([var_str("A"), var_str("C"), var_str("C"), var_str("B")])) &
                .and.assert_equals([2, 3, 4, 1], reverse_sorted_order([var_str("A"), var_str("C"), var_str("B"), var_str("B")])) &
                .and.assert_equals([4, 1, 2, 3], reverse_sorted_order([var_str("B"), var_str("B"), var_str("A"), var_str("C")])) &
                .and.assert_equals([4, 1, 2, 3], reverse_sorted_order([var_str("B"), var_str("A"), var_str("A"), var_str("C")])) &
                .and.assert_equals([3, 4, 1, 2], reverse_sorted_order([var_str("B"), var_str("A"), var_str("C"), var_str("C")])) &
                .and.assert_equals([3, 1, 2, 4], reverse_sorted_order([var_str("B"), var_str("B"), var_str("C"), var_str("A")])) &
                .and.assert_equals([2, 3, 1, 4], reverse_sorted_order([var_str("B"), var_str("C"), var_str("C"), var_str("A")])) &
                .and.assert_equals([2, 1, 3, 4], reverse_sorted_order([var_str("B"), var_str("C"), var_str("A"), var_str("A")])) &
                .and.assert_equals([1, 2, 4, 3], reverse_sorted_order([var_str("C"), var_str("C"), var_str("A"), var_str("B")])) &
                .and.assert_equals([1, 4, 2, 3], reverse_sorted_order([var_str("C"), var_str("A"), var_str("A"), var_str("B")])) &
                .and.assert_equals([1, 3, 4, 2], reverse_sorted_order([var_str("C"), var_str("A"), var_str("B"), var_str("B")])) &
                .and.assert_equals([1, 2, 3, 4], reverse_sorted_order([var_str("C"), var_str("C"), var_str("B"), var_str("A")])) &
                .and.assert_equals([1, 2, 3, 4], reverse_sorted_order([var_str("C"), var_str("B"), var_str("B"), var_str("A")])) &
                .and.assert_equals([1, 2, 3, 4], reverse_sorted_order([var_str("C"), var_str("B"), var_str("A"), var_str("A")]))
    end function
end module
