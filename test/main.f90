! Generated by cart. DO NOT EDIT
program main
    implicit none

    if (.not.run()) stop 1
contains
    function run() result(passed)
        use reverse_sorted_order_test, only: &
                reverse_sorted_order_reverse_sorted_order => &
                    test_reverse_sorted_order
        use sorted_order_test, only: &
                sorted_order_sorted_order => &
                    test_sorted_order
        use string_reverse_sorted_order_test, only: &
                string_reverse_sorted_order_reverse_sorted_order => &
                    test_reverse_sorted_order
        use string_sorted_order_test, only: &
                string_sorted_order_sorted_order => &
                    test_sorted_order
        use veggies, only: test_item_t, test_that, run_tests



        logical :: passed

        type(test_item_t) :: tests
        type(test_item_t) :: individual_tests(4)

        individual_tests(1) = reverse_sorted_order_reverse_sorted_order()
        individual_tests(2) = sorted_order_sorted_order()
        individual_tests(3) = string_reverse_sorted_order_reverse_sorted_order()
        individual_tests(4) = string_sorted_order_sorted_order()
        tests = test_that(individual_tests)


        passed = run_tests(tests)

    end function
end program
