! Copyright (c) 2022 Archaeologic, Inc., Brad Richardson
! This software was developed for the U.S. Nuclear Regulatory Commission (US NRC) under contract # 31310020D0006:
! "Technical Assistance in Support of NRC Nuclear Regulatory Research for Materials, Waste, and Reactor Programs"
module double_precision_array_input_m
    use veggies, only: input_t

    implicit none
    private
    public :: double_precision_array_input_t

    type, extends(input_t) :: double_precision_array_input_t
        private
        double precision, allocatable :: input_(:)
    contains
        private
        procedure, public :: input
    end type

    interface double_precision_array_input_t
        module procedure construct
    end interface
contains
    pure function construct(input) result(array_input)
        double precision, intent(in) :: input(:)
        type(double_precision_array_input_t) :: array_input

        allocate(array_input%input_, source = input)
    end function

    pure function input(self)
        class(double_precision_array_input_t), intent(in) :: self
        double precision, allocatable :: input(:)

        allocate(input, source = self%input_)
    end function
end module
